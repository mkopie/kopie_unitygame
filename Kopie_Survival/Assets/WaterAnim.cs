﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterAnim : MonoBehaviour {

    public static int risecount;
    public static int fallcount;

    // Use this for initialization
    void Start () {
        fallcount = 191;
    }
	
	// Update is called once per frame
	void Update () {
        GameObject plat = GameObject.Find("Water");

        if (risecount < 191)
        {

            plat.transform.localScale = plat.transform.localScale + new Vector3(0f, 0.01f, 0f);

            risecount = risecount + 1;

            if (risecount == 191)
            {
                fallcount = 0;
            }
        }

        if (fallcount < 191)
        {
            plat.transform.localScale = plat.transform.localScale - new Vector3(0f, 0.01f, 0f);

            fallcount = fallcount + 1;

            if (fallcount == 191)
            {
                risecount = 0;
            }
        }
    }
}
