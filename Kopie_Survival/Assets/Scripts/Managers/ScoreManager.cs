﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;
    public static int scoreadder;
    public string magic = "Fire";
    public static int scoretotal;


    Text text;
    Text magtext;
    Text desctext;
    LineRenderer theline;


    void Awake()
    {
        text = GetComponent<Text>();
        GameObject magicdisplay = GameObject.Find("MagicText");
        GameObject magdesc = GameObject.Find("MagicDesc");
        GameObject gunend = GameObject.Find("GunBarrelEnd");

        theline = gunend.GetComponent<LineRenderer>();
        magtext = magicdisplay.GetComponent<Text>();
        desctext = magdesc.GetComponent<Text>();
        score = 3;
        scoreadder = 0;
        magic = "Fire";
        print(theline.material);
    }


    void Update()
    {
        scoretotal = scoreadder + 3;
        text.text = "MANA: " + score + "/" + scoretotal;

        if (score <= 0)
        {
            if (magic == "Fire")
            {
                magic = "Freeze";
                magtext.text = "MAGIC: FREEZE";
                desctext.text = "LOW DAMAGE / FREEZES ENEMIES";
                theline.material.color = Color.blue;
                GameObject.Find("GunBarrelEnd").GetComponent<PlayerShooting>().damagePerShot = 20;
                GameObject.Find("GunBarrelEnd").GetComponent<PlayerShooting>().timeBetweenBullets = 0.2f;
                print(theline.material);


                GameObject blueplat = GameObject.Find("Blue");
                blueplat.transform.localScale = new Vector3(8f, 4f, 1f);

                GameObject redplat = GameObject.Find("Red");
                redplat.transform.localScale = new Vector3(8f, 1f, 1f);
            }
            else if (magic == "Freeze")
            {
                magic = "Thunder";
                magtext.text = "MAGIC: THUNDER";
                desctext.text = "LOW DAMAGE / WEAKENS ENEMIES";
                theline.material.color = Color.yellow;
                GameObject.Find("GunBarrelEnd").GetComponent<PlayerShooting>().damagePerShot = 20;
                GameObject.Find("GunBarrelEnd").GetComponent<PlayerShooting>().timeBetweenBullets = 0.2f;
                print(theline.material);

                GameObject yellowplat = GameObject.Find("Yellow");
                yellowplat.transform.localScale = new Vector3(1f, 4f, 8f);

                GameObject blueplat = GameObject.Find("Blue");
                blueplat.transform.localScale = new Vector3(8f, 1f, 1f);
            }
            else if (magic == "Thunder")
            {
                magic = "Starstorm";
                magtext.text = "MAGIC: STARSTORM";
                desctext.text = "INFINITE DAMAGE / NO EFFECT";
                theline.material.color = Color.black;
                GameObject.Find("GunBarrelEnd").GetComponent<PlayerShooting>().damagePerShot = 1000;
                GameObject.Find("GunBarrelEnd").GetComponent<PlayerShooting>().timeBetweenBullets = 0.5f;
                print(theline.material);

                GameObject blackplat = GameObject.Find("Black");
                blackplat.transform.localScale = new Vector3(1f, 4f, 8f);

                GameObject yellowplat = GameObject.Find("Yellow");
                yellowplat.transform.localScale = new Vector3(1f, 1f, 8f);
            }
            else if (magic == "Starstorm")
            {
                magic = "Fire";
                magtext.text = "MAGIC: FIRE";
                desctext.text = "NORMAL DAMAGE / NO EFFECT";
                theline.material.color = Color.red;
                GameObject.Find("GunBarrelEnd").GetComponent<PlayerShooting>().damagePerShot = 20;
                GameObject.Find("GunBarrelEnd").GetComponent<PlayerShooting>().timeBetweenBullets = 0.1f;
                print(theline.material);

                GameObject redplat = GameObject.Find("Red");
                redplat.transform.localScale = new Vector3(8f, 4f, 1f);

                GameObject blackplat = GameObject.Find("Black");
                blackplat.transform.localScale = new Vector3(1f, 1f, 8f);

                Component[] enemymans;

                enemymans = GameObject.Find("EnemyManager").GetComponents(typeof(EnemyManager));

                foreach (EnemyManager enemy in enemymans)
                {
                    enemy.spawnTime = enemy.spawnTime + 10;
                    print(enemy.spawnTime);
                }
            }

            scoreadder = scoreadder + 1;
            score = 3 + scoreadder;
        }
    }
}
